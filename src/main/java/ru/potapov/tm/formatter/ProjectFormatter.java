package ru.potapov.tm.formatter;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.Formatter;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;

import java.text.ParseException;
import java.util.Collection;
import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
public class ProjectFormatter implements Formatter<Project> {

    @NotNull ServiceLocator serviceLocator;

    public ProjectFormatter(@NotNull ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Project parse(@Nullable final String s, @Nullable Locale locale) throws ParseException {
        Collection<Project> list = serviceLocator.getProjectService().findAll();
        for (Project project : list) {
            if (project.getId().equals(s))
                return project;
        }

        return new Project();
    }

    @Override
    public String print(@NotNull final Project project, @Nullable Locale locale) {
        return project.toString();
    }
}
