package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IUserService;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.repository.IUserRepository;
import ru.potapov.tm.util.SignatureUtil;

import java.util.Collection;
import java.util.UUID;

@Service
@Getter
@Setter
@Transactional
public class UserService extends AbstractService<User> implements IUserService {
//    @Autowired
    @NotNull private IUserRepository repository;

    @Override
    public @Nullable IUserRepository getRepository() {
        return (IUserRepository) super.getRepository();
    }

    public void createPredefinedUsers() {
        @NotNull String hashPass;

        hashPass = SignatureUtil.sign("1", "", 1);
        createUser("user", hashPass, RoleType.User);

        hashPass = SignatureUtil.sign("2", "", 1);
        createUser("admin", hashPass, RoleType.Administrator);

        hashPass = SignatureUtil.sign("test", "", 1);
        createUser("test", hashPass, RoleType.Administrator);
    }

    @NotNull
    @Override
    public User createUser(@Nullable final String name, @Nullable final String hashPass, @Nullable final RoleType role) {
        @NotNull User user = new User();
        user.setRoleType(role);
        user.setLogin(name);
        user.setHashPass(hashPass);
        user.setId(UUID.randomUUID().toString());

        Session session = new Session();
        user.setSessionId(getServiceLocator().getSessionService().generateSession(session,user.getId()).getId());

        getRepository().save( user );
        return user;
    }

    @Override
    public @NotNull Collection<User> findAll() {
        return (Collection<User>) getRepository().findAllUsers();
    }
}
