package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.ITaskService;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.repository.ITaskRepository;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class TaskService extends AbstractService<Task> implements ITaskService {
    @Autowired
    @NotNull
    private ITaskRepository repository;
}
