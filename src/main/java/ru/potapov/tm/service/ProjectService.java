package ru.potapov.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.potapov.tm.api.IProjectService;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.repository.IProjectRepository;

@Service
@Getter
@Setter
@Transactional
@NoArgsConstructor
public class ProjectService extends AbstractService<Project> implements IProjectService {
    @Autowired
    @NotNull
    private IProjectRepository repository;
}
