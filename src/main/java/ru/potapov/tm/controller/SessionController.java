package ru.potapov.tm.controller;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.potapov.tm.api.ServiceLocator;
import ru.potapov.tm.entity.Project;
import ru.potapov.tm.entity.Session;
import ru.potapov.tm.entity.Task;
import ru.potapov.tm.entity.User;
import ru.potapov.tm.enumeration.RoleType;
import ru.potapov.tm.enumeration.Status;

import java.text.SimpleDateFormat;
import java.util.Date;

@Setter
@Getter
@Controller
@ComponentScan(basePackages = "ru.potapov.tm")
public class SessionController {

    @NotNull private final Logger logger = LoggerFactory.getLogger(SessionController.class);

    @Autowired
    @NotNull ServiceLocator serviceLocator;

    public SessionController() {}

    //sessions
    @RequestMapping(value = "/sessions", method = RequestMethod.POST)
    public String saveOrUpdateSession(@ModelAttribute("sessionForm") @Validated @NotNull Session session,
                                   BindingResult result, Model model,
                                   final RedirectAttributes redirectAttributes) {

        logger.debug("saveOrUpdateSession() : {}", session);
        if (result.hasErrors()) {
            populateDefaultModel(model);
            return "sessions/sessionform";
        } else {
            // Add message to flash scope
            redirectAttributes.addFlashAttribute("css", "success");
            if(session.isNew()){
                redirectAttributes.addFlashAttribute("msg", "Session added successfully!");
            }else{
                redirectAttributes.addFlashAttribute("msg", "Session updated successfully!");
            }
            serviceLocator.getSessionService().merge(session);

            populateDefaultModel(model);

            return "sessions/list";
        }
    }

    // show add session form
    @RequestMapping(value = "/sessions/add", method = RequestMethod.GET)
    public String showAddSessionForm(Model model) {

        logger.debug("showAddSessionForm()");

        @NotNull Session session = new Session();
        // set default value
        session.setSignature("");
        session.setDateStamp(new Date().getTime());
        session.setUser(null);
        model.addAttribute("sessionForm", session);

        populateDefaultModel(model);

        return "sessions/sessionform";
    }

    // show update form
    @RequestMapping(value = "/sessions/{id}/update", method = RequestMethod.GET)
    public String showUpdateSessionForm(@PathVariable("id") String id, Model model) {
        logger.debug("showUpdateSessionForm() : {}", id);

        @NotNull final Session session = serviceLocator.getSessionService().findOne(id);
        model.addAttribute("sessionForm", session);

        populateDefaultModel(model);

        return "sessions/sessionform";
    }

    // delete session
    @RequestMapping(value = "/sessions/{id}/delete", method = RequestMethod.GET)
    public String deleteSession(@PathVariable("id") String id, Model model, RedirectAttributes redirectAttributes) {
        logger.debug("deleteSession() : {}", id);

        @NotNull final Session session = serviceLocator.getSessionService().findOne(id);
        if (session != null)
            serviceLocator.getSessionService().remove(session);

        redirectAttributes.addFlashAttribute("css", "success");
        redirectAttributes.addFlashAttribute("msg", "Session is deleted!");

        populateDefaultModel(model);

        return "sessions/list";        
    }

    private void populateDefaultModel(Model model) {
        model.addAttribute("userList", serviceLocator.getUserService().findAll());
        model.addAttribute("projectList", serviceLocator.getProjectService().findAll());
        model.addAttribute("taskList", serviceLocator.getTaskService().findAll());
        model.addAttribute("sessionList", serviceLocator.getSessionService().findAll());

        model.addAttribute("statuses", Status.values());
        model.addAttribute("roles", RoleType.values());
    }
}
