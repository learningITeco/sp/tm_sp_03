package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity
@Getter
@Setter
@Cacheable
@Table(name = "app_session")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity {

    @Id
    @Nullable
    private String id;

    public Session() {
        id = UUID.randomUUID().toString();
    }

    //@OneToOne(mappedBy="session", cascade=CascadeType.ALL)
    //@OneToOne(cascade=CascadeType.ALL)
    @OneToOne
    @JoinColumn(name="user_id")
    @Nullable private User user;

    @NotNull
    private String signature        = "";

    @Column(name = "timestamp")
    @NotNull private long dateStamp  = new Date().getTime();

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    //Check if this is for New of Update
    public boolean isNew() {
        return (getSignature().isEmpty());
    }

}
