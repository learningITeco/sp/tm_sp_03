package ru.potapov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.potapov.tm.enumeration.RoleType;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@Entity
@Cacheable
@Table(name = "app_user")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class User extends AbstractEntity implements Cloneable, Serializable {
    @Id
    @Nullable
    private String id;

    @Column(nullable = false, unique = true)
    @Nullable private String login;

    @Column(name = "passwordHash")
    @Nullable private String hashPass;

    @Column(name = "session_id")
    @Nullable private String sessionId;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    @NotNull private RoleType roleType = RoleType.User;

    public User() {
        id = UUID.randomUUID().toString();
    }

    //Check if this is for New of Update
    public boolean isNew() {
        return (getLogin().isEmpty());
    }

    @Override
    public String toString() {
        return login + " [" + roleType + "]";
    }
}
