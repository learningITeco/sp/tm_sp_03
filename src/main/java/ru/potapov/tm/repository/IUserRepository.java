package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.User;

import java.util.Collection;

@Repository
public interface IUserRepository extends CrudRepository<User, String> {
    @Nullable User findOptionalByLogin(@NotNull final String login);

    @Cacheable("findAllUsers")
    @Query("select e from User e")
    Collection<User> findAllUsers();
}
