package ru.potapov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.potapov.tm.entity.Task;

@Repository
public interface ITaskRepository extends CrudRepository<Task, String> {

    Iterable<Task> findAllByUser(@NotNull final String userId);

    Iterable<Task> findAllByUserIdAndProjectId(@NotNull final String userId, @NotNull final String projectId);

    Task findByName(@NotNull String name);

}
