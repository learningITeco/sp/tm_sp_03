<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">

<div class="container">

	<c:choose>
		<c:when test="${taskForm['new']}">
			<h1>Add task</h1>
		</c:when>
		<c:otherwise>
			<h1>Update task</h1>
		</c:otherwise>
	</c:choose>
	<br />

	<spring:url value="/tasks" var="taskActionUrl" />

	<form:form class="form-horizontal" method="post"
                modelAttribute="taskForm" action="${taskActionUrl}">

		<form:hidden path="id" />

		<spring:bind path="name">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10">
				<form:input path="name" type="text" class="form-control"
                                id="name" placeholder="Name" />
				<form:errors path="name" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="description">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">Description</label>
			<div class="col-sm-10">
				<form:input path="description" class="form-control"
                                id="description" placeholder="Description" />
				<form:errors path="description" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="project">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-sm-2 control-label">Project</label>
            <div class="col-sm-10">
                <form:select path="project" multiple="false">
                    <form:option value="${project}" label="--Select project"/>
                    <form:options items="${projectList}" itemLabel="name" itemValue="id"/>
                </form:select>
>
				<form:errors path="project" class="control-label" />
		  </div>
		</spring:bind>
		
		<spring:bind path="user">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
            <label class="col-sm-2 control-label">User</label>
            <div class="col-sm-10">
                <form:select path="user" multiple="false">
                    <form:option value="${user}" label="--Select user"/>
                    <form:options items="${userList}" itemLabel="login" itemValue="id"/>
                </form:select>
>
				<form:errors path="user" class="control-label" />
		  </div>
		</spring:bind>

		<spring:bind path="dateStart">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">DateStart</label>
			<div class="col-sm-10">
				<form:input path="dateStart" class="form-control"
                                id="dateStart" placeholder="DateStart" />
				<form:errors path="dateStart" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="dateFinish">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">DateFinish</label>
			<div class="col-sm-10">
				<form:input path="dateFinish" class="form-control"
                                id="dateFinish" placeholder="DateFinish" />
				<form:errors path="dateFinish" class="control-label" />
			</div>
		  </div>
		</spring:bind>

		<spring:bind path="status">
		  <div class="form-group ${status.error ? 'has-error' : ''}">
			<label class="col-sm-2 control-label">Status</label>
			<div class="col-sm-10">

                <form:select path="status" multiple="false" name="status">
                    <c:forEach items="${statuses}" var="status">
                        <form:option value="${status}">${status}</form:option>
                    </c:forEach>
                </form:select>

				<form:errors path="status" class="control-label" />
			</div>
		  </div>
		</spring:bind>
				
		<div class="form-group">
		  <div class="col-sm-offset-2 col-sm-10">
			<c:choose>
			  <c:when test="${taskForm['new']}">
			     <button type="submit" class="btn-lg btn-primary pull-right">Add</button>
			  </c:when>
			  <c:otherwise>
			     <button type="submit" class="btn-lg btn-primary pull-right">Update</button>
			  </c:otherwise>
			</c:choose>
		  </div>
		</div>
	</form:form>

</div>

</body>
</html>