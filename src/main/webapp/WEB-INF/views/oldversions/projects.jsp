<%@ taglib prefix="spring" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
    <head>
      <title>Index Page</title>
    </head>

    <body>
        <h2>*** TABLE PROJECTS ***</h2>

        <form:form method="post" action="save.html" modelAttribute="projectForm">
            <table>
                <tr>
                    <th>No.</th>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>USER</th>
                    <th>DESCRIPTION</th>
                    <th>DATE START</th>
                    <th>DATE FINISH</th>
                    <th>STATUS</th>
                </tr>
                <c:forEach items="${projectList}" var="project" varStatus="status">
                    <tr>
                        <td align="center">${status.count}</td>
                        <td><input name="project[${status.index}].id" value="${project.id}"/></td>
                        <td><input name="project[${status.index}].name" value="${project.name}"/></td>
                        <td><input name="project[${status.index}].description" value="${project.description}"/></td>
                        <td><input name="project[${status.index}].user.getLogin()" value="${project.user.getLogin()}"/></td>
                        <td><input name="project[${status.index}].dateStart" value="${project.dateStart}"/></td>
                        <td><input name="project[${status.index}].dateFinish" value="${project.dateFinish}"/></td>
                        <td><input name="project[${status.index}].status" value="${project.status}"/></td>
                    </tr>
                </c:forEach>
            </table>
        <br/>
        <input type="submit" value="Save" />

        </form:form>

    </body>
</html>