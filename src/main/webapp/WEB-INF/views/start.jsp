<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
 pageEncoding="ISO-8859-1"%>
 <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
      <title>Index Page</title>
    </head>

    <body>
        <h2>*** WELCOME TO TASK MANAGER! ***</h2>

       <form:form action="project" method="post"  modelAttribute="mProjects">
         <input type = "submit" name = "action1" value="PROJECTS"/>
       </form:form>

       <form:form action="task" method="post"  modelAttribute="mTasks">
         <input type = "submit" name = "action2" value="TASKS"/>
       </form:form>

       <form:form action="user" method="post"  modelAttribute="mUsers">
         <input type = "submit" name = "action3" value="USERS"/>
       </form:form>

    </body>
</html>